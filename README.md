Description
===========
This code implements a 1Password OPVault reader in Python 3.6 & over.
The full reference for the OPVault format is provided by AgileBits at:

https://support.1password.com/opvault-design

Package ``otp`` implements [TOTP](https://en.wikipedia.org/wiki/Time-based_One-time_Password_algorithm) & 
[HOTP](https://en.wikipedia.org/wiki/HMAC-based_One-time_Password_algorithm) 
code generator classes that only rely on standard python libraries.

I have implemented the OTP code generator code so that all the information 
available inside the vaults is accessible for a programmer with Python knowledge, 
but the idea is that, at some point, I shall implement classes for each 
type of vault item so that instead of having a ``Item`` object filled with 
JSON data, you'd get a specific item type that is easily readable without 
having to transverse too many structures.

Support
=======
This library supports most of the features described in the OPVault
design document:

  * Parsing the OPVault profile file.
  * Parsing band files.
    * Parsing items related to band files (including notes, logins...)
    * Reading attachments and their associated icons for attachment format 1.

The only missing features are attachment format version 2 & folder support.

Information is exposed using python-native constructs,
which means that date/times are exposed as python ``datetime`` objects,
OPVault items are exposed as dictionaries of ``Item`` objects (with the
keys of the dictionaries being strings containing their UUIDs) of the 
opened ``Vault`` and attachments are exposed as dictionaries of 
``Attachment`` objects (with the keys being the attachment UUID) of 
the associated ``Item``s. The files themselves are stored as `byte`
arrays within the ``contents`` attribute of the ``Attachment``
objects.

One thing that is particularly wrong about this implementation is 
that all the available data is unencrypted and remains so for as
long as the vault is unlocked. According to AgileBits, only 
data marked as "overview" should stay unencrypted in memory
and private data (passwords, attachments) should only be unencrypted 
while consumed. I might fix this in a future update.

Dependencies
============

The code depends on the following packages being installed:

* [pycrypto](https://pypi.org/project/pycrypto/)

Test data
=========
You can download a sample vault from agilebits themselves at:

https://cache.agilebits.com/security-kb/

the master password for the vault is `freddy`. Place it under a
folder named "test".

Usage sample
============
For the complete example, please see ``main.py``.

We first import the required modules:
```python
import sys
from pathlib import Path
from vault import Vault, PasswordIncorrect
```

We can now create a new ``Vault`` object by providing its path:
```python
vault = Vault(Path('tests') / 'onepassword_data')
```
At this point, the Vault object should contain a copy of most of the 
encrypted information contained in the OPVault. In order to be able to
access it, you must unlock (decrypt) the vault by providing its master 
password: 

```python
try:
    vault.unlock('freddy')
except PasswordIncorrect:
    sys.stderr.write('Could not unlock the vault with the given password, quitting\n')
    sys.exit(1)
```

All the info in the vault should now be decrypted and accessible. Of 
particular interest is the ``items`` attribute, containing the vault's 
(login, wallet...) items. We can print some info about them:

```python
# Print some info about the vault
for uuid in vault.items.keys():
    print('Item UUID: {}'.format(uuid))
    # Print overview data for this item
    print('Overview:')
    for i in vault.items[uuid].overview:
        print('\t{}: {}'.format(i, vault.items[uuid].overview[i]))

    print('\n')

    # Print encrypted details data
    print('Details:')
    for i in vault.items[uuid].details:
        print('\t{}: {}'.format(i, vault.items[uuid].details[i]))

    print('\n')
```

For each item we can also print the names and UUID values for the 
associated attachments:

```python
    # Print info about attachments
    for attachment_uuid in vault.items[uuid].attachments:
        print('\t{}: {}'.format(attachment_uuid,
                                vault.items[uuid].attachments[attachment_uuid].filename))
```

Locking the vault forgets the encrypted information and removes the
encryption keys from memory. Python is particularly loose with this,
so the code also forces a garbage collection cycle, but I cannot 
really guarantee that all the information will be erased from memory.

```python
vault.lock()
```

Warning
=======
This code is not complete and is probably just wrong in many levels,
don't use it anywhere near any real vault of yours.

**The code will most likely set your computer on fire when used**; please
use your OS's [routines](https://www.haiku-os.org/legacy-docs/bebook/TheKernelKit_SystemInfo.html#is_computer_on_fire) to check for this. That 
said, should you choose to run or study it, please drop me a note to 
[joseba.gar@gmail.com](mailto:joseba.gar@gmail.com) if you find it useful.
 
Also
====
I've written this code because I wanted to experiment a bit with encryption
and the OPVault format seemed like a good place to start. I would not have 
been able to write it  if Agilebits hadn't developed 1Password or had not
decided to extensively document the OPVault file format.
 
1Password is a wonderful product in a world where privacy is a growing concern.
Please consider purchasing a license from them:

https://1password.com/