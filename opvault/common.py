#  Copyright 2018 Joseba Echevarria García <joseba.gar@gmail.com>
#
#  This file is part of pyopvault.
#
#  pyopvault is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyopvault is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyopvault.  If not, see <http://www.gnu.org/licenses/>.

"""
Info taken from:
https://support.1password.com/opvault-design/
"""

import hmac
import struct
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes


class NotAnOPData01(ValueError):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


def opdata01_read(byte_data, key, hmac_key):
    """
    Parse the contents of an opdata field, returning the decrypted bytes

    https://support.1password.com/opvault-design/#opdata01
    """
    # Check the magic number (first 8 bytes) of the binary data
    if byte_data[:8] != b'opdata01':
        raise NotAnOPData01('Given data does not contain an opdata01 block')

    # The next 8 bytes contain a 64 bit little-endian unsigned
    # integer with the size of the plaintext in bytes
    size = int(struct.unpack_from('<Q', byte_data, 8)[0])

    # The last 256bits (32bytes) of binary data are the MAC, we check it
    if not verify_hmac(byte_data[:-32], hmac_key, byte_data[-32:]):
        raise NotAnOPData01('Byte data does not match verification sum')

    # We now decrypt the encrypted data with the crypto key
    decrypted = decrypt_data(byte_data[32:-32], key, byte_data[16:32])

    return decrypted[-size:]


def verify_hmac(crypted, hmac_key, mac):
    """
    Verify that the MAC for the given crypted data matches the expected value

    Parameters
    ----------
    crypted : bytes
              Encrypted data in a binary form
    hmac_key : bytes
               HMAC key
    mac : bytes
          Expected MAC value

    Returns
    -------
    result : boolean
    """
    return mac == hmac.new(hmac_key, crypted, 'sha256').digest()


def decrypt_data(crypted, key, iv):
    cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=default_backend())
    decryptor = cipher.decryptor()
    return decryptor.update(crypted) + decryptor.finalize()

