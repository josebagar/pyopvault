#  Copyright 2018 Joseba Echevarria García <joseba.gar@gmail.com>
#
#  This file is part of pyopvault.
#
#  pyopvault is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyopvault is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyopvault.  If not, see <http://www.gnu.org/licenses/>.

"""
Code design to interact with opvault attachments.

Info taken from:
https://support.1password.com/opvault-design/
"""

import json
import struct
import base64
from datetime import datetime
from opvault.common import opdata01_read, decrypt_data


class AttachmentException(ValueError):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class NotAnAttachment(AttachmentException):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class AttachmentVersionUnsupported(AttachmentException):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class TamperedAttachment(AttachmentException):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class Attachment:
    """
    Attachment, see:

    https://support.1password.com/opvault-design/#attachments
    """

    created = None
    updated = None
    uuid = None
    item_uuid = None
    transaction_timestamp = None
    contents_size = None
    external = None
    overview = None
    icon = None
    contents = None
    __overview_crypted = None
    __icon_crypted = None
    __contents_crypted = None
    filename = None

    def __init__(self, byte_data):
        # Check the magic number (first 6 bytes) of the binary data
        if byte_data[:7] != b'OPCLDAT':
            raise NotAnAttachment('Given data does not contain an opvault attachment')

        # Format version (the only available version is 1 as of this writing)
        # if byte_data[7] != 0x01:
        #     raise AttachmentVersionUnsupported('Only attachment format 0x01 is supported '
        #                                        '(got 0x{:02x})'.format(byte_data[7]))

        # Attachment format version
        self.version = byte_data[7]

        # Read the metadata size (in bytes)
        metadata_size = int(struct.unpack_from('<H', byte_data, 8)[0])
        # Read the icon size (in bytes)
        icon_size = int(struct.unpack_from('<I', byte_data, 12)[0])

        # Read the metadata itself
        metadata = json.loads(byte_data[16:16+metadata_size].decode('utf-8'))
        __created_at = metadata.get('createdAt', None)
        self.created = datetime.fromtimestamp(__created_at) if __created_at else None
        __updated_at = metadata.get('updatedAt', None)
        self.updated = datetime.fromtimestamp(__updated_at) if __updated_at else None
        self.uuid = metadata['uuid']
        self.item_uuid = metadata['itemUUID']
        __tx_timestamp = metadata.get('txTimestamp', None)
        self.transaction_timestamp = (datetime.fromtimestamp(__tx_timestamp)
                                      if __tx_timestamp else None)
        self.trashed = metadata.get('trashed', False)
        self.contents_size = metadata.get('contentsSize', None)
        __overview = metadata.get('overview', None)
        self.__overview_crypted = base64.b64decode(metadata['overview']) if __overview else None
        self.external = metadata.get('external', None)

        # Read the encrypted icon data
        self.__icon_crypted = byte_data[16+metadata_size:16+metadata_size+icon_size]
        # Finally read the encrypted contents of the attachment
        self.__contents_crypted = byte_data[16+metadata_size+icon_size:]

    def unlock(self, item_key, item_hmac, overview_key):
        """
        Decrypt attachment data
        """
        self.contents = opdata01_read(self.__contents_crypted, item_key, item_hmac)
        if self.contents_size is not None and len(self.contents) != self.contents_size:
            raise TamperedAttachment('Attachment contains incorrect data or has been tampered with')
        if len(self.__icon_crypted) > 0:
            self.icon = opdata01_read(self.__icon_crypted, item_key, item_hmac)
        if self.__overview_crypted:
            self.overview = decrypt_data(self.__overview_crypted[16:], overview_key,
                                         self.__overview_crypted[:16])
        # TODO: HACK, HACK HAAAAACK!!! I don't know how to parse the overview...
        start_pos = self.overview.find(b'{')
        end_pos = self.overview.find(b'}') + 1
        self.filename = json.loads(self.overview[start_pos:end_pos].decode('utf-8')).get('filename',
                                                                                         None)

    def lock(self):
        """
        Forget attachment private data
        """
        self.contents = None
        self.icon = None
        self.overview = None
        self.filename = None
