#  Copyright 2018 Joseba Echevarria García <joseba.gar@gmail.com>
#
#  This file is part of pyopvault.
#
#  pyopvault is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyopvault is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyopvault.  If not, see <http://www.gnu.org/licenses/>.

"""
Code design to interact with vault items.

Info taken from:
https://support.1password.com/opvault-design/
"""

import sys
import hmac
import json
import base64
import logging
from datetime import datetime
from json import JSONDecodeError

from opvault.attachment import Attachment, AttachmentException
from opvault.common import opdata01_read, decrypt_data, verify_hmac, NotAnOPData01


CATEGORY_LOGIN = 1
CATEGORY_CREDIT_CARD = 2
CATEGORY_SECURE_NOTE = 3
CATEGORY_IDENTITY = 4
CATEGORY_PASSWORD = 5
CATEGORY_TOMBSTONE = 99
CATEGORY_SOFTWARE_LICENSE = 100
CATEGORY_BANK_ACCOUNT = 101
CATEGORY_DATABAE = 102
CATEGORY_DRIVER_LICENSE = 103
CATEGORY_OUTDOOR_LICENSE = 104
CATEGORY_MEMBERSHIP = 105
CATEGORY_PASSPORT = 106
CATEGORY_REWARDS = 107
CATEGORY_SSN = 108
CATEGORY_ROUTER = 109
CATEGORY_SERVER = 110
CATEGORY_EMAIL = 111


class NotAVaultItem(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class TamperedItem(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class Item:
    """
    Vault item, see:

    https://support.1password.com/opvault-design/#items
    """
    attachments = None
    category = None
    uuid = None
    created = None
    updated = None
    transaction_time = None
    trashed = None
    fave = None
    folder = None
    overview = None
    details = None
    __details_crypted = None
    __key_crypted = None
    __overview_crypted = None
    __hmac = None
    __data_crypted = None

    def __init__(self, data, attachments):
        try:
            self.uuid = data['uuid'].upper()
            self.category = int(data['category'])
            self.created = datetime.fromtimestamp(data['created'])
            self.updated = datetime.fromtimestamp(data['updated'])
            self.transaction_time = datetime.fromtimestamp(data['tx'])
            self.__details_crypted = base64.b64decode(data['d'])
            self.__key_crypted = base64.b64decode(data['k'])
            self.__overview_crypted = base64.b64decode(data['o'])
            self.__hmac = base64.b64decode(data['hmac'])
            self.__data_crypted = data
            self.trashed = data.get('trashed', False)
            self.fave = data.get('fave', False)
            self.folder = data.get('folder', None)
        except (KeyError, ValueError):
            raise NotAVaultItem('Some of the required fields were not found in the login item')

        # Load attachments as a dictionary
        self.attachments = {}
        for attachment in attachments:
            try:
                attachment = Attachment(attachment)
                self.attachments[attachment.uuid] = attachment
            except AttachmentException as e:
                logging.warning('Skipping attachment with error "{}"'.format(e))
                pass

    def lock(self):
        """Forget private data about this data & its attachments"""
        for uuid in self.attachments:
            self.attachments[uuid].lock()

        self.overview = None
        self.details = None

    def unlock(self, master_key, master_hmac, overview_key, overview_hmac):
        """Read and store private data"""

        # Compute the hmac value from the login entries and then compare
        # it to the one in the login item, if they do not match either
        # the decryption key is invalid or the data has been tampered with
        h = hmac.new(overview_hmac, digestmod='sha256')
        for key in sorted(self.__data_crypted.keys()):
            if key == 'hmac':
                continue
            elif key == 'trashed':
                h.update(b'trashed')
                if self.trashed:
                    h.update(b'1')
                else:
                    h.update(b'0')
            else:
                h.update(key.encode('utf-8'))
                h.update(str(self.__data_crypted[key]).encode('utf-8'))

        # Check if the HMAC value we computed is
        # the same as the one in the login item
        if h.digest() != self.__hmac:
            raise TamperedItem('MAC check failed for {}'.format(self.uuid))

        self.overview = json.loads(opdata01_read(self.__overview_crypted,
                                                 overview_key,
                                                 overview_hmac).decode('utf-8'))

        # Check that the HMAC stored with the key is valid
        # If it weren't, we should have found about it above
        if not verify_hmac(self.__key_crypted[:-32], master_hmac, self.__key_crypted[-32:]):
            raise TamperedItem('Login key in the element is not valid')

        # Decrypt the item key & hmac
        key_data = decrypt_data(self.__key_crypted[16:-32], master_key, self.__key_crypted[:16])
        item_key = key_data[:32]
        item_hmac = key_data[32:]

        # Read the details
        self.details = json.loads(opdata01_read(self.__details_crypted,
                                                item_key, item_hmac).decode('utf-8'))

        # Unlock the attachment files
        for uuid in self.attachments.keys():
            try:
                self.attachments[uuid].unlock(item_key, item_hmac, overview_key)
            except (NotAnOPData01, JSONDecodeError, UnicodeDecodeError):
                sys.stderr.write('Could not unlock attachment "{}" since it is not in OPDate01 format\n'.format(uuid))
