#  Copyright 2018 Joseba Echevarria García <joseba.gar@gmail.com>
#
#  This file is part of pyopvault.
#
#  pyopvault is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyopvault is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyopvault.  If not, see <http://www.gnu.org/licenses/>.

"""
Info taken from:
https://support.1password.com/opvault-design/
"""

import gc
import json
import base64
import hashlib
from pathlib import Path
from datetime import datetime
from opvault.common import opdata01_read, NotAnOPData01
from opvault.item import Item, NotAVaultItem, TamperedItem


class NotAnOPVault(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class NotAProfileFile(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class NotABandFile(Exception):
    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class PasswordIncorrect(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class TamperedVault(Exception):
    def __init__(self, message):

        # Call the base class constructor with the parameters it needs
        super().__init__(message)


class Vault:
    items = {}
    uuid = None
    created_at = None
    updated_by = None
    updated = None
    profile_name = None
    password_hint = None
    __salt = None
    __iterations = None
    __master_key_crypted = None
    __overview_key_crypted = None
    __master_key = None
    __master_hmac = None
    __overview_key = None
    __overview_hmac = None
    __derived_encryption_key = None
    __mac_key = None

    def __init__(self, path, profile='default'):
        if not isinstance(path, Path):
            path = Path(path)
        self.path = path / profile
        # Check if this looks like an OPVault file at all
        required_files = ['profile.js']
        for file in required_files:
            if not (self.path / file).is_file():
                raise NotAnOPVault('"{}" does not seem to contain an OPVault'.format(self.path))

        # Read the profile data
        self._parse_profile(self.path / 'profile.js')

        # Read the band files
        for band_path in sorted(self.path.glob('band_[0-9A-F].js')):
            try:
                self._parse_band(band_path)
            except (NotABandFile, NotAVaultItem):
                raise NotAnOPVault('"{}" does not seem to contain valid band data'.format(band_path))

    def unlock(self, password):
        """
        Try to unlock the vault using the provided master password
        This process aims to obtain the Master Encryption Key & the Overview Key

        https://support.1password.com/opvault-design/#key-derivation
        """
        # The first 256 bits (32 bytes) of the key contain the derived encryption key,
        # whereas the final 256 bits (32 bytes) contain the derived MAC key
        key = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                  self.__salt,
                                  self.__iterations)
        self.__derived_encryption_key = key[:32]
        self.__mac_key = key[32:]

        # We read the master & overview keys from the opdata01 field
        # If the reading process raise a NotAnOpData01 exception, the
        # password is incorrect (or has been tempered with) and we must
        # stop
        try:
            __master_key_data = opdata01_read(self.__master_key_crypted,
                                              self.__derived_encryption_key,
                                              self.__mac_key)
            __overview_key_data = opdata01_read(self.__overview_key_crypted,
                                                self.__derived_encryption_key,
                                                self.__mac_key)
        except NotAnOPData01:
            raise PasswordIncorrect('Could not open the vault with the provided password')

        # The data we've retrieved is, however, are not the keys themselves
        # We must obtain the SHA512 sum of the data and that'll contain the keys
        master_keys = hashlib.sha512(__master_key_data).digest()
        overview_keys = hashlib.sha512(__overview_key_data).digest()
        self.__master_key = master_keys[:32]
        self.__master_hmac = master_keys[-32:]
        self.__overview_key = overview_keys[:32]
        self.__overview_hmac = overview_keys[-32:]

        # Read the private data for each one of the vault items
        try:
            for uuid in self.items:
                self.items[uuid].unlock(self.__master_key, self.__master_hmac,
                                        self.__overview_key, self.__overview_hmac)
        except TamperedItem:
            self.lock()
            raise TamperedVault('Key data inconsistent for item with UUID "{}"'.format(uuid))

    def lock(self):
        """Lock the keychain by forgetting about the encryption keys"""
        self.__derived_encryption_key = None
        self.__mac_key = None
        self.__master_key = None
        self.__overview_key = None
        self.__master_hmac = None
        self.__overview_hmac = None

        # Forget about the private data for each one of the vault items
        for uuid in self.items:
            self.items[uuid].lock()

        # Call garbage collection
        gc.collect()

    def _parse_profile(self, profile_path):
        # Read the profile data and check that it starts as expected
        profile_data = profile_path.read_text(encoding='utf-8')
        if not profile_data.startswith('var profile='):
            raise NotAProfileFile('"{}" does not seem to contain valid profile data'.format(profile_path))

        # Convert the data into a dict and process it
        data = json.loads(profile_data[12:-1])
        self.uuid = data['uuid']
        self.created_at = datetime.fromtimestamp(data['createdAt'])
        self.updated_by = data['lastUpdatedBy']
        self.updated = datetime.fromtimestamp(data['updatedAt'])
        self.profile_name = data['profileName']
        self.__salt = base64.b64decode(data['salt'])
        self.__master_key_crypted = base64.b64decode(data['masterKey'])
        self.__overview_key_crypted = base64.b64decode(data['overviewKey'])
        self.__iterations = int(data['iterations'])
        if 'passwordHint' in data.keys():
            self.password_hint = data['passwordHint']

    def _parse_band(self, band_path):
        """
        Read & parse band file
        """
        band = band_path.stem[-1]
        band_data = band_path.read_text(encoding='utf-8')
        if not band_data.startswith('ld(') or not band_data.endswith(');'):
            raise NotABandFile('Cannot understand "{}" as a band file'.format(band_path))

        login_items = json.loads(band_data[3:-2])
        for uuid in sorted(login_items):
            if uuid[0] != band:
                raise NotABandFile('UUID "{}" does not match band "{}"'.format(uuid, band))

            # Find the list of attachments associated with the vault item
            attachments = [i.read_bytes() for i in self.path.glob('{}_*.attachment'.format(uuid))]
            self.items[uuid] = Item(login_items[uuid], attachments)

