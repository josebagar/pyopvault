#  Copyright 2018 Joseba Echevarria García <joseba.gar@gmail.com>
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.


import hmac
from urllib.parse import urlparse, parse_qs
from datetime import timedelta, datetime, timezone


class OTP:
    def __init__(self, secret=None, url=None, digits=6):
        """
        Time-based One Time Password

        Class implementing a Time-based One Time Password that can
        generate code based on the shared secret or a URL.

        You can either create it by providing the shared secret
        directly or a URL, but one must be provided.

        If both `shared_secret` & `url` are provided, `url` takes
        precedence.

        Parameters
        ----------
        secret : string or bytes
                 Secret shared between the client & the server.

                 * If given a string, it will be processed as if it were base32
                 encoded and filled with `=` symbols in case its length is
                 not a multiple of 8.
                 * If given bytes, it will be used directly.
        url : string or bytes
              URL used
        digits : int, optional
                 Number of digits in each TOTP code. Defaults to 6.
        """
        # Input data sanity check
        if secret is None and url is None:
            raise ValueError('Either `shared_secret` or `url` must be provided')

        # Read the secret from either the given value or extract it from the URL
        _secret = None
        if url is not None:
            # Read the secret key from the URL
            p = urlparse(url)
            params = parse_qs(p.query)
            if 'secret' in params.keys():
                _secret = self.__byte_secret(params['secret'][0])
            else:
                raise ValueError('Could not extract the secret key from the given URL')

        if _secret is None:
            if isinstance(secret, str):
                _secret = self.__byte_secret(secret)
            elif isinstance(secret, bytes):
                _secret = secret
            else:
                raise RuntimeError('The secret key must be provided as either a string or bytes')

        # Store the user-provided params
        self.__secret = _secret
        self.__digits = digits

    def value(self, counter, digest_algorithm='sha1'):
        """
        Get the OTP value

        Return the OTP value for the given time and digest algorithm.

        Parameters
        ----------
        counter : int
                  Counter for the OTP. This should increase both in the
                  server and in the client each time the user logs in.
        digest_algorithm : str, optional
                           Digest algorithm to use. Defaults to SHA1.
                           This method will take any value accepted
                           by :py:class:`hmac.new`

        Returns
        -------
        otp_value : str
                     The OTP code
        """
        # Input data sanity check
        if not isinstance(counter, int):
            raise TypeError('`counter` must be an integer')

        # Compute the HMAC digest of the given counter
        msg = counter.to_bytes(8, byteorder='big')
        h = hmac.new(self.__secret, msg=msg, digestmod=digest_algorithm).digest()

        # Take the 4 least significant bytes of the HMAC as an offset
        int_hash = int.from_bytes(h, byteorder='big')
        offset = (int_hash & 0b1111)
        # Select 31 bits from h, starting at bit offset+1
        n_shift = 8 * (len(h) - offset) - 32
        mask = 0b1111111111111111111111111111111 << n_shift
        truncated = (int_hash & mask) >> n_shift

        return ("{:0" + str(self.__digits) + "}").format(truncated % (10 ** self.__digits))

    @staticmethod
    def __byte_secret(secret):
        missing_padding = len(secret) % 8
        if missing_padding != 0:
            secret += '=' * (8 - missing_padding)

        return base64.b32decode(secret, casefold=True)


class TOTP(OTP):
    def __init__(self, secret=None, url=None, digits=6,
                 validity=timedelta(seconds=30),
                 reference=datetime(year=1970, month=1, day=1, tzinfo=timezone.utc)):
        """
        Time-based One Time Password

        Class implementing a Time-based One Time Password that can
        generate code based on the shared secret or a URL.

        You can either create it by providing the shared secret
        directly or a URL, but one must be provided.

        If both `shared_secret` & `url` are provided, `url` takes
        precedence.

        Parameters
        ----------
        secret : string or bytes
                 Secret shared between the client & the server
        url : string or bytes
              URL used
        digits : int, optional
                 Number of digits in each TOTP code. Defaults to 6.
        validity : timedelta, optional
                   Period for which each code is valid.
                   If not provided, each code will be valid for 30s.
        reference : datetime, optional
                    Reference date & time instant. If not provided,
                    1970/01/01 UTC (the Unix epoch) will be used.
        """
        super(TOTP, self).__init__(secret=secret, url=url, digits=digits)

        # Store the user-provided params
        self.__validity = validity
        self.__reference = reference

    def value(self, instant=None, digest_algorithm='sha1'):
        """
        Get the TOTP value

        Return the TOTP value for the given time and digest algorithm.

        If `instant` is not provided, the code will compute the current
        TOTP value (according to the system time).

        Parameters
        ----------
        instant : datetime object
                  The time for which the TOTP value should be computed.
                  You probably want to provide an "aware" datetime object
                  or make sure that the reference datetime is provided
                  in the same timezone as this one.
        digest_algorithm : str, optional
                           Digest algorithm to use. Defaults to SHA1.

        Returns
        -------
        totp_value : str
                     The TOTP key
        """
        # Input data sanity check
        if instant is None:
            instant = datetime.now(timezone.utc)

        # Convert the instant to the number of periods elapsed since
        # the reference time/date
        counter = int((instant - self.__reference).total_seconds() / self.__validity.total_seconds())

        return super(TOTP, self).value(counter, digest_algorithm=digest_algorithm)


if __name__ == '__main__':
    import base64


    def main():
        secret = '6lep3kmj3m34af2jusozt4tdhlukvzpc'
        url = 'otpauth://totp/Google%3Ajosebagar%40gmail.com?secret=6lep3kmj3m34af2jusozt4tdhlukvzpc&issuer=Google'

        t = TOTP(secret=secret)
        t2 = TOTP(url=url)
        print(t.value())
        print(t2.value())


    main()
