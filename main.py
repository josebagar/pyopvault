#!/usr/bin/env python3

#  Copyright 2018 Joseba Echevarria García <joseba.gar@gmail.com>
#
#  This file is part of pyopvault.
#
#  pyopvault is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyopvault is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyopvault.  If not, see <http://www.gnu.org/licenses/>.

import sys
import getpass
from pathlib import Path
from opvault.vault import Vault, PasswordIncorrect

vault = Vault(Path('tests') / 'onepassword_data')

master_password = None
try:
    master_password = Path('master.password').open('rt', encoding='utf-8').readline()
except IOError:
    master_password = getpass.getpass(prompt='Please, enter your master password: ')

try:
    vault.unlock(master_password.strip())
except PasswordIncorrect:
    sys.stderr.write('Could not unlock the vault with the given password, quitting\n')
    sys.exit(1)

# Print some info about the vault
for uuid in vault.items.keys():
    print('Item UUID: {}'.format(uuid))
    print('Overview:')
    for i in vault.items[uuid].overview:
        print('\t{}: {}'.format(i, vault.items[uuid].overview[i]))

    print('\n')

    # Print encrypted details data
    print('Details:')
    for i in vault.items[uuid].details:
        print('\t{}: {}'.format(i, vault.items[uuid].details[i]))

    print('\n')

    # Print info about attachments
    print('Attachments:')
    for attachment_uuid in vault.items[uuid].attachments:
        print('\t{}: {}'.format(attachment_uuid,
                                vault.items[uuid].attachments[attachment_uuid].filename))
    print('\n')

vault.lock()

sys.stdout.write('Everything is fine! :)\n')
sys.exit(0)
