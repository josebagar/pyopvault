#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García <joseba.gar@gmail.com>
#
# This file is part of pyopvault.
#
# pyopvault is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pyopvault is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with pyopvault.  If not, see <http://www.gnu.org/licenses/>.

import unittest
from pathlib import Path
from opvault.vault import Vault, PasswordIncorrect, NotAnOPVault


class TestVault(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """
        Create a `Vault` object from the test vault
        """
        try:
            cls.vault = Vault(Path('..') / 'tests' / 'onepassword_data')
        except NotAnOPVault:
            # Maybe the code was run from the main directory
            # instead of from the tests directory?
            cls.vault = Vault(Path('.') / 'tests' / 'onepassword_data')

    def testVaultIncorrect(self):
        """
        Test that trying to open an incorrect vault results in
        the code raising the `NotAnOPVault` exception
        """
        self.assertRaises(NotAnOPVault, Vault, Path('.'))

    def testPasswordIncorrect(self):
        """
        Test that trying to open the test vault with an incorrect password
        results in the `PasswordIncorrect` exception
        """
        self.assertRaises(PasswordIncorrect, self.vault.unlock, 'incorrect_password')

    def testPasswordLocking(self):
        """
        Test that trying to unlock & re-lock a vault does not raise any exception
        """
        self.vault.unlock('freddy')
        self.vault.lock()

    def testVaultItems(self):
        """
        Test that the test vault has the right item UUIDs and
        that one of them contains the expected data
        """
        self.vault.unlock('freddy')

        # Check for all the item UUIDs
        expected_keys = {'0C4F27910A64488BB339AED63565D148', '0EDE2B13D7AC4E2C9105842682ACB187',
                         '13C8E12AC8E54B1F873BAB0824E521BC', '1C7D72EFA19A4EE98DB7A9661D2F5732',
                         '27DCFA2810B24083A3ECC7CEABC7C0A9', '2A632FDD32F5445E91EB5636C7580447',
                         '358B7411EB8B45CD9CE592ED16F3E9DE', '372E1D51AA1D44CB9F17D8AA70ADA9A6',
                         '468B1E24F93B413DAD57ABE6F1C01DF6', '4E36C011EE8348B1B24418218B04018C',
                         '5ADFF73C09004C448D45565BC4750DE2', '67979020CCA54120BAFA2742C3F23F2B',
                         '72366D161D9E43D98E58EB801DAD1EF8', '8445A23B5740455DA360FEA379C3CC90',
                         'A2D44483145F4B41A849FE5FEA4B504D', 'AE272805811C450586BA3EDEAEF8AE19',
                         'D06307ADA44C4031BA2FF4B174DE79CB', 'D1820AA8CB534AC6A4B5A2C0263FD3B2',
                         'D8F79F17D6384808848B213EB4946ECA', 'E0D293D29B10483F8DFDAC72ED0BE5C0',
                         'E482B70C038D4DD78A0940728FA737BF', 'EC0A40400ABB4B16926B7417E95C9669',
                         'F2DB5DA3FCA64372A751E0E85C67A538', 'F3707FA58EA7480884BC6A662658E039',
                         'F5F099B210F248348E22934DDC3338B2', 'F7883ADDE5944B349ABB5CBEC20F39BE',
                         'F78CEC04078743B6975511A6FDDBED7E', 'FD2EADB43C4F4FC7BEB35A1692DDFDEA',
                         'FF445AB1497241A28812363154E1A738'}
        self.assertTrue(self.vault.items.keys() == expected_keys)

        # Check one particular UUID
        item = self.vault.items['FF445AB1497241A28812363154E1A738']
        self.assertEqual(item.overview['title'], 'Johnny Appleseed Society')
        self.assertEqual(item.overview['ainfo'], 'Wendy Appleseed')
        self.assertEqual(item.details['sections'][0]['fields'][1]['v'],
                         'http://www.urbana.edu/resources/community/johnny-appleseed/appleseed-society.html')
        attachment_uuid = '16684B74F26145169EC03B950DC68E95'
        self.assertTrue(attachment_uuid in item.attachments)
        attachment = item.attachments[attachment_uuid]
        self.assertEqual(attachment.filename, 'document_20_4_252.jpg')

        self.vault.lock()


if __name__ == '__main__':
    unittest.main()
